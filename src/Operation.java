public class Operation {
    private int position;
    private char value;

    public Operation(int position, char value) {
        this.position = position;
        this.value = value;
    }

    public int getPosition() {
        return position;
    }

    public char getValue() {
        return value;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public void setValue(char value) {
        this.value = value;
    }
}
