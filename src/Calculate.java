import java.util.InputMismatchException;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class Calculate {
    private Scanner scanner = new Scanner(System.in);
    private double a;
    private double b;
    private double c;
    private String operation;
    private String row;

    public void  readRow()
    {
        System.out.println("Vvedite virazhenie");
        row = scanner.next();
    }

    public void process()
    {
        Queue <Operation> operations = new LinkedList<>();
        for (int i=0;i<row.length();i++)
        {
            char currentChar = row.charAt(i);
            switch(currentChar){
                case '+':
                case '-':
                case '*':
                case '/':
                    operations.add(new Operation(i, currentChar));
            }
        }

        while (!operations.isEmpty()){
//            find priority
            Operation currentOperation = new Operation();

            String leftNumber = "";

            String rightNumber = "";

            String result = "";

            row = row.replaceAll(leftNumber + currentOperation.getValue() + rightNumber, result);
            
            process();
        }
    }

    public double getC() {
        return c;
    }

    public void readOperation() {
        System.out.println("Введите действие");
        operation = scanner.next();
     /*   if (operation.equals("/") ) dif();
        else if (operation.equals("*")) imul();
        else if (operation.equals("+")) add();
        else if (operation.equals("-")) min();
     */
    }

    public void test() {
        switch (operation) {
            default:
                throw new RuntimeException("неизвестный оператор");
            case "+":
                add();
                break;
            case "-":
                min();
                break;
            case "*":
                imul();
                break;
            case "/":
                dif();
                break;
        }
    }

    public void readA() {

        System.out.println("Введите значение a");
        //  if ((scanner.nextDouble() > -1.7E+308) || (scanner.nextDouble() < 1.7E+308))
        //     throw new RuntimeException("a не входит в диапазон");
        try {
            a = scanner.nextDouble();

        } catch (InputMismatchException e) {
            //throw new RuntimeException("Введите корректное значение а");
            System.out.println("Введите корректное значение а");
            scanner = new Scanner(System.in);
            readA();
        }

        // return a;
    }

    public void readB() {
        System.out.println("Введите значение b");
        try {
            b = scanner.nextDouble();
        } catch (InputMismatchException e) {
            throw new RuntimeException("Введите корректное значение b");
        }
    }


    public void dif()//double a, double b)
    {
        if (b == 0)
            throw new RuntimeException("На 0 делить нельзя");
        else {
            c = a / b;
            System.out.println(c);
        }


    }

    public void imul()
    {
        c=a*b;
    }

    public void add()
    {
        c=a+b;
    }

    public void min()
    {
        c=a-c;
    }

}
